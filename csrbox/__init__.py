# See LICENSE.incore for details

"""Top-level package for RISC-V CSR Box Generator."""

__author__ = """IIT-Madras, InCore Semiconductors Pvt Ltd"""
__email__ = 'shakti.iitm@gmail.com'
__version__ = '1.7.0'
