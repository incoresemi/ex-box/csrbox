# See LICENSE.incore for details

"""The setup script."""

import os
import codecs
import pip
from setuptools import setup, find_packages

import csrbox

# Base directory of package
here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()
def read_requires():
    with open(os.path.join(here, "csrbox/requirements.txt"),"r") as reqfile:
        return reqfile.read().splitlines()

#Long Description
with open("README.rst", "r") as fh:
    readme = fh.read()

setup_requirements = [ ]

test_requirements = [ ]

setup(
    name='csrbox',
    version='1.7.0',
    description="RISC-V CSR-Box Generator",
    long_description=readme + '\n\n',
      classifiers=[
          "Programming Language :: Python :: 3.6",
          "License :: OSI Approved :: BSD License",
          "Development Status :: 4 - Beta"
      ],
    url='https://gitlab.com/incoresemi/ex-box/csrbox',
    author="IIT-Madras, InCore Semiconductors Pvt. Ltd.",
    author_email='shakti.iitm@gmail.com',
    license='BSD-3-Clause',
    packages=find_packages(),
    package_dir={'csrbox': 'csrbox'},
    package_data={
        'csrbox': [
            'requirements.txt', 
            'attributes.yaml'
            ]
        },
    install_requires=read_requires(),
    python_requires='>=3.6.0',
      entry_points={
        'console_scripts': [
            'csrbox=csrbox.main:cli',
        ],
    },
    include_package_data=True,
    keywords='csrbox, riscv',
    test_suite='tests',
    tests_require=test_requirements,
    zip_safe=False,
)
