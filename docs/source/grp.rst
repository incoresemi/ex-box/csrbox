###################
YAML Specifications
###################

This section provides details of the Group spec YAML file that need to be provided by the user.


.. _grp_yaml_spec:

Group yaml dependencies
=======================
All the csrs can be grouped together in any form, as per the requirements of the user. But there are some special cases of dependencies , shadows and action methods for subfields.

+--------------------------+--------------------------------------------------------------------+
|     CSRS                 |         Depends on                                                 |
+==========================+====================================================================+
|                          |                                                                    |
|  **SHADOW CSRS**         |                                                                    |
|                          |                                                                    |
|    1. HPMCOUNTER         |          - Machine Counterpart, MHPMCOUNTER                        |  
|    2. CYCLE              |          - Machine Counterpart, MCYCLE                             | 
|    3. INSTRET            |          - Machine Counterpart, MINSTRET                           |
+--------------------------+--------------------------------------------------------------------+
|                          |                                                                    |
|**SUB-FIELD DEPENDENCIES**|                                                                    |
|                          |                                                                    |
|                          |                                                                    |
|1. FRM                    |      - Sub-field of Mstatus, FS                                    |
|2. FFLAGS                 |      - Sub-field of Mstatus, FS                                    | 
|3. FCSR                   |      - Sub-field of Mstatus, FS                                    |
+--------------------------+--------------------------------------------------------------------+
|                          |                                                                    |
|**MACHINE COUNTERS**      |   Corresponding Machine event csr and machine countinhibit csr     |
+--------------------------+--------------------------------------------------------------------+                          
| **PHYSICAL MEMORY**      |   Corresponding Physical memory protection configuration sub-field | 
| **PROTECTION ADDRESS**   |   csrs                                                             |
|                          |                                                                    |
+--------------------------+--------------------------------------------------------------------+

**NOTE**
 * The above dependencies can be understood as follows:

     * It is recommended that the csr and the dependent csr are put in the same group
     * If not, csrbox takes care by extracting the dependent csrs using special action methods
     
     
 * The shadow csrs are completely dependent on their machine counterpart and are not instantiated as separate csrs themselves.

 * Flags like frm, fflags and fcsr depend on the machine status csr and its subfield fs bits. Make sure to keep the flags and mstatus in the same group.

 * The pmpaddr csrs depend on the corresponding sub-fields of the pmp configuration csrs. 


Constraints
-----------
1. Mhpmcounter, cycle, instret all have upper 32 bit 'h' counterparts that have to be placed in the same group, for xlen=32. The upper and lower counterparts have to be present when invkoing the respective counter, cycle or instret.
2. Csrs must not be repeated in the same group
3. No group should be filled with NULL elements of csrs.
4. Make sure the csrs do not get repeated in several groups
5. Make sure to follow the spacing formats of yaml
6. Make sure to keep all the supervisor csr and their machine counterparts in the same group.


