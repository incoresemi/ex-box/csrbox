########
Overview
########

This is the general flow chart for csrbox:

.. image:: CSRBOX1.png


The user is required to provide 2 YAML files as input:

1. **ISA Spec**: This YAML file is meant to capture the ISA related features implemented by the user.  This is first fed into riscv-config python and the standard isa spec is finally put into the csrbox.
2. **Grouping Spec**: This YAML file is meant to capture how the csrs are grouped together by the user. Details of this input file can be found here : :ref:`grp_yaml_spec`.

Current CSR support 
===================
The current version of Csrbox- 1.0.1 supports all machine mode csrs, corresponding supervisor mode and user mode csrs that depend only on 'U' extension . For the complete list of csrs, refer here : :ref:`list_of_csrs`.

Working
=======

**INPUT**:

The isa spec is first passed through riscv config to get the standard isa spec as the intermediate input to csrbox.

**PROCESS**:

   The csrs present in both the grouping yaml as well std isa spec and are consecutively checked for the :
   
                   * Size of the csr ( if subfields exists, the check is for highest msb value) 
                   * Description of csr
                   * Address of the csr for decoder
                   * Shadow csrs

   **Attribute Yaml**
     The attribute yaml is used to automate the process of checking for shadows, dependent csrs, special case action methods and subfields internally.
     The general structure includes :
   
           * Value method
           * Action method
              - definition argument
              - declaration body
           * Dependencies
               - interface method definition
               - register instantiation
               - module declaration
               - csrbox main mkconnection() {to connect the value method from the group csr is being extracted from}
               
     **Note: Any additions to the attributes yaml should be done in terms of bluespec code and not as running commentary**
               
   **CSR Decoder**
    The decoder is a bluespec demultiplexer like function that checks the validity of the given csr address. The job of the function is to compare the given address with all the csrbox supported address . If it is able to match, a bool variable becomes valid, else is turned false.
      
   **CSR Defines**  
    It consists of automatically generated macros for all csrs corresponding with their addresses. This is done after reading the isa yaml file for the right addresses.
   
   **WARL Support**
    The function first splits the warl string into the condition and the legalizing strings. A regular expression is then applied to both strings         separately to extract parameters and create bluespec code.


**OUTPUT**:
 All output files are automatically placed under the work directory with the intermediate isa file in it and the bluespec files in a separate folder. These bluespec files are generated after successful bluesim and verilog compilation.
 
 The files generated are :
 
               * Csrbox main file to moderate the groups
               * Individual group bsv files
               * CSR Decoder
               * CSR Defines
               * CSR types
               * ISA checked file

       
   **CSR flow in Daisy Chain**
    All core requests consists of the following :
    
               * Address of the csr
               * Data to written
               * Two bit operands(funct3) to determine if data is to write/read
               
   Data flows from one group to another in a daisy chain manner until it hits the right group.
   If there is a miss in the first group , using an actionvalue method and a fifo the core request is forwarded to the next node.
     
   Once there is hit, a CSR Response is recorded consisting of:
   
               * Bool variable to indicate the hit
               * Data that has been read      

.. _list_of_csrs:

**LISTS OF SUPPORTED CSRS**
============================
+-------------------------------+                
|     **MACHINE CSRS**          |
+===============================+              
|      MVENDORID                |
+-------------------------------+
|      MARCHID                  | 
+-------------------------------+ 
|      MIMPID                   |
+-------------------------------+
|      MHARTID                  |
+-------------------------------+
|      MISA                     |
+-------------------------------+
|      MTVEC                    |  
+-------------------------------+
|      MEDELEG                  |
+-------------------------------+
|      MIDELEG                  |
+-------------------------------+
|      MIE                      |
+-------------------------------+
|      MCOUNTEREN               |
+-------------------------------+
|      MSTATUS                  |
+-------------------------------+
|      MIP                      | 
+-------------------------------+
|      MSCRATCH                 |
+-------------------------------+
|      MEPC                     |
+-------------------------------+
|      MCAUSE                   |
+-------------------------------+
|      MTVAL                    |  
+-------------------------------+
|      PMPCFG0                  |
+-------------------------------+
|      PMPCFG1                  |
+-------------------------------+
|      PMPCFG2                  |
+-------------------------------+
|      PMPCFG3                  |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |  
+-------------------------------+
|         .                     |
+-------------------------------+
|      PMPCFG14                 |
+-------------------------------+
|      PMPCFG15                 |
+-------------------------------+
|      PMPADDR0                 |
+-------------------------------+
|      PMPADDR1                 |
+-------------------------------+
|      PMPADDR2                 |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|      PMPADDR63                |
+-------------------------------+
|      MCYCLE                   |
+-------------------------------+
|      MINSTRET                 |
+-------------------------------+
|      MHPMCOUNTER3             |
+-------------------------------+
|      MHPMCOUNTER4             |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|      MHPMCOUNTER31            |
+-------------------------------+
|      MCYCLEH                  |
+-------------------------------+
|      MINSTRETH                |
+-------------------------------+
|      MHPMCOUNTER3H            |
+-------------------------------+
|      MHPMCOUNTER4H            |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|      MHPMCOUNTER31H           |
+-------------------------------+
|      MCOUTNINHIBIT            |
+-------------------------------+
|      MHPMEVENT3               |
+-------------------------------+
|      MHPMEVENT4               |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|      MHPMEVENT31              |
+-------------------------------+          
        
        
+-------------------------------+
|     **USER CSRS**             |
+===============================+
|      USTATUS                  |
+-------------------------------+
|      UIP                      |
+-------------------------------+
|      UIE                      |
+-------------------------------+
|      UTVEC                    |
+-------------------------------+
|      UEPC                     |
+-------------------------------+
|      UTVAL                    |
+-------------------------------+
|      UCAUSE                   |
+-------------------------------+
|      USCRATCH                 |
+-------------------------------+
|      CYCLE                    |
+-------------------------------+
|      INSTRET                  |
+-------------------------------+
|      HPMCOUNTER3              |
+-------------------------------+
|      HPMCOUNTER4              |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|      HPMCOUNTER31             |
+-------------------------------+
|      CYCLEH                   |
+-------------------------------+
|      INSTRETH                 |
+-------------------------------+
|      HPMCOUNTER3H             |
+-------------------------------+
|      HPMCOUNTER4H             |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|         .                     |
+-------------------------------+
|      HPMCOUNTER31H            |
+-------------------------------+
|      TIME                     |
+-------------------------------+
|      TIMEH                    |
+-------------------------------+
|      FFLAGS                   |
+-------------------------------+
|      FRM                      |
+-------------------------------+
|      FCSR                     |
+-------------------------------+

        
+-------------------------------+
|     **SUPERVISOR CSRS**       |
+===============================+
|      SSTATUS                  |
+-------------------------------+
|      SEDELEG                  |
+-------------------------------+
|      SIDELEG                  |
+-------------------------------+
|      SIE                      |
+-------------------------------+
|      STVEC                    |
+-------------------------------+
|      SSCRATCH                 |
+-------------------------------+
|      SEPC                     |
+-------------------------------+
|      SCAUSE                   |
+-------------------------------+
|      STVAL                    |
+-------------------------------+
|      SIP                      |
+-------------------------------+
|      SATP                     |
+-------------------------------+
|      SSCRATCH                 |
+-------------------------------+
|      SCOUNTEREN               |
+-------------------------------+
|      SIDELEG                  |
+-------------------------------+
|      SEDELEG                  |
+-------------------------------+

