.. See LICENSE.incore for details

#####################
Licensing and Support
#####################

Please review the LICENSE.* files in the repository for licensing details.

For more information about support, customization and other IP developments
please write to info@incoresemi.com.
