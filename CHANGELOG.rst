CHANGELOG
=========
This project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[1.7.0] - 2022-08-23
  - minor indentation beautification of bsv generated code
  - updated test yamls
  - introducing optimization for all csrs which are read-only constant 0
  - size of pmpaddr is always physical_addr_sz - 2
  - csrs in isa yaml missing in grouping yaml with throw errors
  - use the new warl_class from riscv-config 3.0.0

[1.6.13] - 2022-06-22
  - fix ci for pypi deployment

[1.6.12] - 2022-06-22
  - fix mstatus.gva setting condition when taking trap in machine mode.

[1.6.11] - 2022-06-07
  - fix pypi deployment CI

[1.6.10] - 2022-06-07
  - added logic to generate function which can dynamically control flushing the pipeline for csr ops

[1.6.9] - 2022-05-21
  - Added probes for frm and fflags. 

[1.6.8] - 2022-05-14
  - adding csr access/permission check functions in csrbox_decoder.bsv

[1.6.7] - 2022-05-09
  - for trap taken in HS mode, update hstatus_gva only when rg_virtual is set.

[1.6.6] - 2022-04-09
  - use lv_trap_type instead of lv_trapmode for setting gva

[1.6.5] - 2022-04-09
  - gva should only be set when the tval is updated with a virtual address
  - support for tval2 usage in hypervisor mode for traps

[1.6.4] - 2022-03-30
  - Fixed SATP mode update condition for SV57.

[1.6.3] - 2022-03-01
  - added support for providing a response if the csr has been updated or not. This signal is
    available only when rtldump macro is enabled

[1.6.2] - 2022-02-28
  - misa extension should supress all changes to misa when compressed is being disabled but the csr
    instruction is at a 2 byte boundary.
  - fixed read/write ports of the CREG of minstret for 32-bit mode

[1.6.1] - 2021-12-08
  - methods to access mpv and gva in mstatush should only exist when hypervisor is enabled

[1.6.0] - 2021-12-10
  - hypervisor support done
  - fixed debugger support
  - improved trap and xret methods for configurability
  - fix for module attributes between core-requests and trap/xret routines
  - created trap and xret routines for 32-bit hypervisor

[1.5.5] - 2021-10-07
  - for minstreth the value and action methods now will read from port 0 of the counter
  - fixed addition of conflict_free attributes in bsv for the minstret value methods.

[1.5.4] - 2021-09-19
  - change variable from XLEN to \`xlen in probe function

[1.5.3] - 2021-06-30
 - the increment for minstret is now a variable argument coming from the top-level. This will enable
   incrementing minstret based on issue/commit width of a processor.

[1.5.2] - 2021-06-23
 - adding support for no-inline compile macros for bsv modules.
 - reversed sequence of updates for minstret. CSR writes to minstret will preceed the increment in
   the same cycle.

[1.5.1] - 2021-06-11
  - changed size of cause argument for mav_up_on_trap method to be a compile macro instead of being
    hardwired to 6

[1.5.0] - 2021-05-24
  - adding support for shadow_type handling
  - fixed handling of external interrupt sources
  - added support for custom interrupt support
  - changed mWire() to mkDWire() to avoid unnecessary scheduling conflicts
  - added --checked option in cli. This command assumes the input yamls are already checked and
    avoids involing riscv-config. To be used purely for developers.

[1.4.0] - 2021-04-06
  - fix value-method instantiation issue.
  - fixed wr_events update in stations having mhpmcounters

[1.3.3] - 2021-03-30
  - fix seip logic issue as per privilege spec.

[1.3.2] - 2021-03-05
  - for the csr_op field in the attributes yaml file, only one of the entries should be enabled.
    Thus when the first enabled entry is observed exit from loop.

[1.3.1] - 2021-03-05
  - added logger statements
  - change request fifo1 to default 2 entry fifo. Fifo1 prevents ma_core_ready from firing in the subsequent cycle.

[1.3.0] - 2021-02-25
  - majority of the fields in the attributes which can be customized now support the conditional
    synatx as well
  - added debug support in trap and declaring new set of csrs
  - created global variable like debug_enabled, compressed_enabled and supervisor_enabled which
    allow conditional checking of nodes in the attributes file
  - adding debug spec to the CLI

[1.2.2] - 2021-02-05
  - updated MANIFEST file to include attributes.yaml

[1.2.1] - 2021-02-05
  - ensure the fs is dirtied each time the fcsr, fflags or frm csrs are written to.
  - fixed handling of ro_variable subfields. They will be instantiated as regular registers.
  - enabled value_methods for sie and sip in the attributes file.

[1.2.0] - 2021-01-13
  - changes for MISA csr to account for pc alignment when switching off C extension
  - new function to create wlrl registers
  - adding misa checks in csrbox_decoder
  - using creg for maintaining priority of writes for instret and cycle counters
  - created a csr to string function similar to spike
  - csrbox now also generates a probe_function which can be useful in test-benches

[1.1.0] - 2021-01-05
  - added support to generate custom methods for pmps, counters and events.
  - adding conflict_free attributes.
  - added support for instantiating custom csrs.
  - null/empty element groups can be declared.
  - warl definition function updated to handle more corner cases and dependencies.

[1.0.2] - 2020-10-23
  - distribute attributes.yaml in pypi package
  - updated docs

[1.0.1] - 2020-10-23
  - added attributes file for capturing custom requirements of each csr.
  - using click based command line interface.
  - added changelog.rst and contributing.rst.
  - split a one major function into multiple smaller specific functions.
  - added documentation of the functions and installation and usage.
  - added support for warl deduction and generation.
  - support for shadow csrs done.


[0.0.3] - 2020-07-17
  - set up CSR-BOX with risc-v config and adjusted the number of machine csrs according to the schema
  - added edge cases errors
  - typo fixes and requirements fix
  - CSR-BOX set up with grouping and isa yaml

